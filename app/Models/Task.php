<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Task extends Model
{
    use HasFactory;

    protected static $unguarded = true;

    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }

}
