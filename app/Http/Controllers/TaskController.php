<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Card;
use App\Models\Desk;
use App\Models\DeskList;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class TaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @param Task $task
     * @param TaskRequest $request
     * @return JsonResponse
     */
    public function store(Desk $desk, DeskList $list, Card $card, Task $task, TaskRequest $request): JsonResponse
    {
        $card->tasks()->create($request->validated());
        return $this->responseMessage('New task successfully created', ResponseAlias::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @param Task $task
     * @return JsonResponse
     */
    public function show(Desk $desk, DeskList $list, Card $card, Task $task): JsonResponse
    {
        return $this->responseResourceful(TaskResource::class, $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @param Task $task
     * @param TaskRequest $request
     * @return JsonResponse
     */
    public function update(Desk $desk, DeskList $list, Card $card, Task $task, TaskRequest $request): JsonResponse
    {
        $task->update($request->validated());

        return $this->responseMessage('Task successfully updated', ResponseAlias::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Desk $desk
     * @param DeskList $list
     * @param Card $card
     * @param Task $task
     * @return JsonResponse
     */
    public function destroy(Desk $desk, DeskList $list, Card $card, Task $task): JsonResponse
    {
        $task->delete();

        return $this->responseMessage($task->name . 'has been deleted', ResponseAlias::HTTP_OK);
    }
}
