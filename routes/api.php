<?php

use App\Http\Controllers\Api\V1\CardController;
use App\Http\Controllers\Api\V1\DeskController;
use App\Http\Controllers\Api\V1\DeskListController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
    'desks'             => DeskController::class,
    'desks.lists'       => DeskListController::class,
    'desks.lists.cards' => CardController::class,
    'desks.lists.cards.tasks' => TaskController::class
]);
