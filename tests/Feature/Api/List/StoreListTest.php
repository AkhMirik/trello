<?php

namespace Tests\Feature\Api\List;

use App\Models\Desk;
use App\Models\DeskList;
use Arr;
use Tests\TestCase;

class StoreListTest extends TestCase
{
    public function test_store_a_list()
    {
        $desk = Desk::inRandomOrder()->first()->lists()->inRandomOrder()->first()->id;
        $this->postJson(route('desks.lists.store', $desk), [
            'name' => 'Mirsaid'
        ])
            ->assertCreated();

        $this->assertDatabaseHas('desk_lists', ['name' => 'Mirsaid']);
    }
}
