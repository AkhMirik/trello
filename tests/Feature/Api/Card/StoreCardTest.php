<?php

namespace Tests\Feature\Api\Card;

use App\Models\Desk;
use Tests\TestCase;

class StoreCardTest extends TestCase
{
    public function test_store_a_card()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $this->postJson(route('desks.lists.cards.store', [$desk->id, $list->id]), [
            'name' => 'new Card'
        ])->assertCreated();

        $this->assertDatabaseHas('cards', [
            'name' => 'new Card'
        ]);
    }
}
