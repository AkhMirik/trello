<?php

namespace Tests\Feature\Api\Card;

use App\Models\Desk;
use App\Models\DeskList;
use Tests\TestCase;

class IndexCardTest extends TestCase
{
    public function test_show_a_card()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();

        $this->getJson(route('desks.lists.cards.show', [$desk->id, $list->id, $card->id]))
            ->assertOk()->assertJsonData([
                'id' => $card->id
            ]);
    }
}
