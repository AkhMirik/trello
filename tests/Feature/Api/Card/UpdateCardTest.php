<?php

namespace Tests\Feature\Api\Card;

use App\Models\Card;
use App\Models\Desk;
use Tests\TestCase;

class UpdateCardTest extends TestCase
{
    public function test_update_a_card()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();
        $changes = [
            'name' => 'card test'
        ];

        $this->patchJson(route('desks.lists.cards.update', [$desk->id, $list->id, $card->id]), $changes)->assertOk();

        $this->assertDatabaseHas('cards', $changes);

        $this->assertTrue(Card::whereName($changes['name'])->exists());
    }
}
