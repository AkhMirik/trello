<?php

namespace Tests\Feature\Api\Desk;

use App\Models\Desk;
use Tests\TestCase;

class DeleteDeskTest extends TestCase
{
    public function test_delete_a_desk()
    {
        $desk = Desk::inRandomOrder()->first()->id;
        $this->assertDatabaseHas('desks', ['id' => $desk]);

        $this->deleteJson(route('desks.destroy', $desk))->assertOk();

        $this->assertDatabaseMissing('desks', ['id' => $desk]);
    }
}
